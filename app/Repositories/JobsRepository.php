<?php

namespace App\Repositories;

use DB;
use App\Jobs;

class JobsRepository {

    // Fetch jobs
    public function getJobs($request)
    {
        return Jobs::when($request->category, function($query) use ($request){
            return $query->where('category', $request->category);
        })
        ->get();
    }

    /*
     * Query builder scope to list neighboring locations 
     * within a given distance from a given location
     */
    public function nearLocations($request)
    {
        // unit in km
        $unit = 6371;
        $lat = (float) $request->latitude;
        $lng = (float) $request->longitude;
        $radius = (double) $request->radius;
        return Jobs::having('distance','<=',$radius)
                    ->select(DB::raw("*,
                                ($unit * ACOS(COS(RADIANS($lat))
                                    * COS(RADIANS(latitude))
                                    * COS(RADIANS($lng) - RADIANS(longitude))
                                    + SIN(RADIANS($lat))
                                    * SIN(RADIANS(latitude)))) AS distance")
                    )
                    ->orderBy('distance','asc')
                    ->get();
    }

    // return jobs based keyword params.
    public function search($request)
    {
        return Jobs::where('title', 'like', "%{$request->keyword}%")
        ->orWhere('description', 'like', "%{$request->keyword}%")
        ->get();
    }

}
