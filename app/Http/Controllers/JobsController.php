<?php
namespace App\Http\Controllers;

use App\Jobs;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\JobsRequest;
use App\Repositories\JobsRepository;

class JobsController extends Controller
{
    /**
     * @var jobsRepository
     */
    private $jobsRepository;
    protected $request;
    protected $job;
    
    /**
     *
     * @param Request $request
     * @param Product $job
     */
    public function __construct(Request $request, Jobs $job, JobsRepository $jobsRepository) {
        $this->request = $request;
        $this->job = $job;
        $this->jobsRepository = $jobsRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $jobs = $this->jobsRepository->getJobs($this->request);
        
        return response()->json([
            'data'   => $jobs,
            'status' => Response::HTTP_OK
        ]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobsRequest $request) {
       
        try {
            $this->job->create($request->all());
            return response()->json(['status' => Response::HTTP_CREATED]);
        } catch (\Exception $e) {
            Log::debug($e);
            throw new HttpResponseException(response()->error());
        }
         
    }

    public function getLocation()
    {
        try{
            $jobs = $this->jobsRepository->nearLocations($this->request);
    
            return response()->json([
                'data'   => $jobs,
                'status' => Response::HTTP_OK
            ]);
        } catch (\Exception $e) {
            Log::debug($e);
            throw new HttpResponseException(response()->error());
        }
    }

    public function search(Request $request)
    {
        $jobs = $this->jobsRepository->search($this->request);
        
        return response()->json([
            'data'   => $jobs,
            'status' => Response::HTTP_OK
        ]);
        
    }
    
   
}
